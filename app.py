from flask import Flask, jsonify
app = Flask(__name__)

count = 0

@app.route('/')
def index():
    return 'Welcome to Python Sample Application for testing!'

@app.route('/hello')
def hello():
    greeting = "Hello world! This is atest"
    return greeting

@app.route('/count', methods=["GET"])
def getCount():
    global count
    count += 1
    return str(count)

if __name__ == "__main__":
    app.run(debug=True,port=8080)
